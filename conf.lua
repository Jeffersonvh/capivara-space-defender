function love.conf(t)
	t.title = "Capivara Space Defender" -- Titulo que vai na janela
	t.version = "0.9.2"         -- Versão em que o jogo foi feito e será executado
	--t.window.width = 800        -- largura da tela da tela.
	--t.window.height = 600  		-- Altura da tela
	t.window.fullscreen = true
	t.console = false
	t.identity = "Save and Load"
	t.window.icon = "pictures/icon.png"
	t.window.borderless = false
end