function enemytime_load()

	inimigoImg = love.graphics.newImage('pictures/inimigo.png')
	inimigoVImg = love.graphics.newImage('pictures/InimigoVertical.png')
	tiroInimigoImg = love.graphics.newImage('pictures/bulletInimigo.png')
	asteroideImg = love.graphics.newImage('pictures/asteroide.png')

	podeAtiraI=true
	tempoMaxTiroI=0.6
	tempoTiroI = tempoMaxTiroI

	tempoMaxInimigo = 0.4
	tempoInimigo = tempoMaxInimigo

	tempoMaxInimigoV = 5
	tempoInimigoV = tempoMaxInimigoV

	tempoMaxInimigoH = 8
	tempoInimigoH = tempoMaxInimigoH

	tempoMaxAsteroide = 3.3
	tempoAsteroide = tempoMaxAsteroide

	tempoMaxEspecial = 20
	tempoEspecial = tempoMaxEspecial

	tempoMaxInv = 3
	tempoInv = tempoMaxInv
end

function enemy_vertical(dt)
	--Inimigo que vem na vertical
	tempoInimigo = tempoInimigo - (1 * dt)
	if tempoInimigo < 0 then
		tempoInimigo = tempoMaxInimigo

		--Criando e colocando inimigo na tabela
		numeroAleatorio = math.random(10, love.graphics.getWidth() - 10)
		novoInimigo = { x = numeroAleatorio, y = -10, img = inimigoImg, tipo=1}
		table.insert (inimigos, novoInimigo)
	end
end

function enemy_diagonal(dt)
	--Inimigo que vem em diagonal
	tempoInimigoV = tempoInimigoV - (1 * dt)
	if tempoInimigoV < 0 then
		tempoInimigoV = tempoMaxInimigoV

		--Criando e colocando inimigo na tabela
		novoInimigo = { x = 800, y = -50, img = inimigoVImg, tipo=21}
		table.insert (inimigos, novoInimigo)
		--Criando e colocando inimigo na tabela
		novoInimigo = { x = -50, y = -50, img = inimigoVImg, tipo=22}
		table.insert (inimigos, novoInimigo)
	end
end

function enemy_horizontal(dt)
	--Inimigo que vem na horizontal
	tempoInimigoH = tempoInimigoH - (1 * dt)
	if tempoInimigoH < 0 then
		tempoInimigoH = tempoMaxInimigoH

		--Criando e colocando inimigo na tabela
		novoInimigo = { x = -50, y = 100, img = inimigoVImg, tipo=3}
		table.insert (inimigos, novoInimigo)
	end
end

function enemy_asteroide(dt)
	--Tempo para criar o asteroide
	tempoAsteroide = tempoAsteroide - (1 * dt)
	if tempoAsteroide < 0 then
		tempoAsteroide = tempoMaxAsteroide

		--Criando e colocando asteroide na tabela
		numeroAleatorio = math.random(10, love.graphics.getWidth() - 10)
		novoAsteroide = { x = numeroAleatorio, y = -10, img = asteroideImg }
		table.insert (asteroides, novoAsteroide)
	end
end

function enemy_update(dt)

	for i, inimigo in ipairs(inimigos) do
		
		--De cima para baixo
		if inimigo.tipo == 1 then
			inimigo.y = inimigo.y + (200 * dt)
		end
		
		--Em diagonal para esquerda
		if inimigo.tipo == 21 then
			inimigo.x=inimigo.x-(200 * dt)
			inimigo.y=inimigo.y+(300 * dt)
		end
		
		--Em diagonal para Direita
		if inimigo.tipo == 22 then
			inimigo.x=inimigo.x+(200 * dt)
			inimigo.y=inimigo.y+(300 * dt)
		end
		
		--Em horizontal para a Direita
		if inimigo.tipo == 3 then
			inimigo.x=inimigo.x+(200 * dt)
			
			if podeAtiraI == true then
			novoTiro = { x = inimigo.x + (inimigo.img:getWidth()/2), y = inimigo.y, img = tiroInimigoImg }
			table.insert(tirosInimigo, novoTiro)
			podeAtiraI = false
			tempoTiroI = tempoMaxTiroI
			end
		end
		
		if inimigo.x > 900 then -- Remove o inimigo que esta fora da tela
			table.remove(inimigos, i)
		end

		if inimigo.y > 850 then -- Remove o inimigo que esta fora da tela
			table.remove(inimigos, i)
		end
	end

	--Cria asteroide
	for i, asteroide in ipairs(asteroides) do
		asteroide.y = asteroide.y + (230 * dt)

		if asteroide.y > 850 then -- Remove o asteroide que esta fora da tela
			table.remove(asteroides, i)
		end
	end

end

function enemy_draw()
	for i, tiroInimigo in ipairs(tirosInimigo) do
		love.graphics.draw(tiroInimigo.img, tiroInimigo.x, tiroInimigo.y)
	end

	for i, asteroide in ipairs(asteroides) do
		love.graphics.draw(asteroide.img, asteroide.x, asteroide.y)
	end

	for i, inimigo in ipairs(inimigos) do
		love.graphics.draw(inimigo.img, inimigo.x, inimigo.y)
	end
end