--Biblioteca para sprites
local anim8 = require 'anim8'
local image, animation
require("menu")
require("enemy")
require("player")
require("boss")
require("maps")

debug = true


--Declarar para não repetir no código
pause = false
win = false
--Tempo para aperecer o boss
	tempo_boss_time = 20
-- Tempo de transisão de uma tela para outra
	tempo_parado = false

--Mapas
espaco= { x=0, y=-900, velocidade=30, img = nil }	
		
--Imagens
tiroImg = nil
tiroInimigoImg = nil
inimigoImg = nil
inimigoVImg = nil
asteroideImg = nil
Bullet2x = nil
maisTiro = nil
--Audios
somTiro = nil

--Tabelas
jogadorSpr = {} --Tabela do Sprite do jogador
tirosInimigo = {} --Tabela de tiros dos inimigos
tiros = {} --Tabela de tiros
inimigos = {} -- Tabela de inimigos
asteroides = {} -- Tabela de Asteroides
explosoes = {} -- Tabela de explosao
destrocoAs = {} -- Tabela de explosao
especiais = {} --Tabela de especiais

--Checar a colição de dois objetos
function ChecaColissao(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

--Carregando imagens...
function love.load(arg)
	level = 1;
	mapa_load()
	enemytime_load()
	player_load()
	boss_load()
	botao_load()
	Bullet2x = love.graphics.newImage('pictures/bullet2x.png')
	maisTiro = love.graphics.newImage('pictures/maisTiro.png')
	
	Musica = love.audio.newSource("sounds/Musica.mp3")
	Batalha = love.audio.newSource("sounds/Batalha.mp3")


	--Sprites explosao
	image = love.graphics.newImage('pictures/explosao.png')
  	local g = anim8.newGrid(100, 100, image:getWidth(), image:getHeight())
  	animation = anim8.newAnimation(g('1-5',1), 0.1)

end

--Atualizando o jogo...
function love.update(dt)

	botao_update(dt)

	if tempo_parado == true then
		love.timer.sleep(5)
		tempo_parado = false
  	end
	map_update(dt)
	--Cria um arquivo se não existir e carrega a melhor pontuação se ja existir
	if not love.filesystem.exists("score.sav") then
    arquivo = love.filesystem.newFile("score.sav")
    arquivo:open('w')
    arquivo:write(0)
    arquivo:close()
  	end
  	--Load the save file state
  	arquivo = love.filesystem.newFile("score.sav")
  	melhorPontuacao = arquivo:read()
  	arquivo:close()


	if pause == false then
	
	player_update(dt)
	boss_update(dt , jogador.x)

	--Tempo para disparar o tiro do inimigo
	tempoTiroI = tempoTiroI - (1 * dt)
	if tempoTiroI < 0 then
		podeAtiraI = true
	end
	
	--Tempo invencibilidade
	tempoInv = tempoInv  - (1 * dt)
	if tempoInv < 0 then
		jogador.inv = false
	end

	--Tempo para criar especial
	tempoEspecial = tempoEspecial - (1 * dt)
	if tempoEspecial < 0 then
		tempoEspecial = tempoMaxEspecial
		numEspecial = math.random(1, 2)
		--Criando o especial
		if numEspecial == 1 then 
			numeroAleatorio = math.random(60, love.graphics.getWidth() - 60)
			novoEspecial = { x = numeroAleatorio, y = -10, img = Bullet2x, tipo=1}
			table.insert (especiais, novoEspecial)
		end
		if numEspecial == 2 then 
			numeroAleatorio = math.random(60, love.graphics.getWidth() - 60)
			novoEspecial = { x = numeroAleatorio, y = -10, img = maisTiro, tipo=2}
			table.insert (especiais, novoEspecial)
		end
	end

	--Tempo para criar o inimigo
	enemy_vertical(dt)
	enemy_diagonal(dt)
	enemy_horizontal(dt)
	enemy_asteroide(dt)
	enemy_update(dt)
	

	for i, tiro in ipairs(tirosInimigo) do
		tiro.y = tiro.y + (250 * dt)

		if tiro.y > 850 then -- Remove o projetil que esta fora da tela
			table.remove(tirosInimigo, i)
		end
	end

	for i, especial in ipairs(especiais) do
			especial.y = especial.y + (200 * dt)
			if especial.y > 850 then -- Remove o inimigo que esta fora da tela
			table.remove(especial, i)
			end
	end


	for i, inimigo in ipairs(inimigos) do
		for j, tiro in ipairs(tiros) do
			if ChecaColissao(inimigo.x, inimigo.y, inimigo.img:getWidth(), inimigo.img:getHeight(), tiro.x, tiro.y, tiro.img:getWidth(), tiro.img:getHeight()) then
				table.remove(tiros, j)
				table.remove(inimigos, i)
				
				if inimigo.tipo == 1 then
					pontos = pontos + 50
				end
				if inimigo.tipo == 21 or 22 then
					pontos = pontos + 150
				end
				if inimigo.tipo == 3 then
					pontos = pontos + 200
				end

				novaExplosao={x = inimigo.x, y = inimigo.y, tempo=0.4}
				table.insert(explosoes, novaExplosao)
			end
		end
		if jogador.inv == false then
			if ChecaColissao(inimigo.x, inimigo.y, inimigo.img:getWidth(), inimigo.img:getHeight(), jogador.x, jogador.y, jogador.img:getWidth(), jogador.img:getHeight()) 
			and vivo then
				if shields.shield < 1 then
					vidas.vida= vidas.vida-1
				elseif shields.shield > 0 then
					shields.shield = shields.shield-1
				end
				jogador.inv = true
				tempoInv  = tempoMaxInv
				table.remove(inimigos, i)
				jogador.x = love.graphics:getWidth()/2-25
				jogador.y = love.graphics:getHeight()-80
				if vidas.vida == 0 then
					vivo = false
				end
			end
		end
	end

	for i, tiro in ipairs(tirosInimigo) do
		if jogador.inv == false then	
			if ChecaColissao(tiro.x, tiro.y, tiro.img:getWidth(), tiro.img:getHeight(), jogador.x, jogador.y, jogador.img:getWidth(), jogador.img:getHeight()) 
				and vivo then
				if shields.shield < 1 then
					vidas.vida= vidas.vida-1
				elseif shields.shield > 0 then
					shields.shield = shields.shield-1
				end
				jogador.inv = true
				tempoInv  = tempoMaxInv
				table.remove(tirosInimigo, i)
				jogador.x = love.graphics:getWidth()/2-25
				jogador.y = love.graphics:getHeight()-80
				if vidas.vida == 0 then
					vivo = false
				end
			end
		end
	end

	for i, especial in ipairs(especiais) do
		if ChecaColissao(especial.x, especial.y, especial.img:getWidth(), especial.img:getHeight(), jogador.x, jogador.y, jogador.img:getWidth(), jogador.img:getHeight()) then
			if especial.tipo == 1 then
			tempoMaxTiro=0.2
			end
			if especial.tipo == 2 then
			doisProjetil=true
			end
			table.remove(especiais, i)
		end
	end

	if boss_vivo == true then
		for j, tiro in ipairs(tiros) do
			if ChecaColissao(boss.x, boss.y, boss.img:getWidth(), boss.img:getHeight(), tiro.x, tiro.y, tiro.img:getWidth(), tiro.img:getHeight()) then
				table.remove(tiros, j)
				boss.life = boss.life - 1
			end
		end
	end

	for i, explosao in ipairs(explosoes) do
		
		explosao.tempo = explosao.tempo - (1 * dt)
		if explosao.tempo < 0 then
			table.remove(explosoes, i)
		end

	end
	animation:update(dt)
	

	for i, asteroide in ipairs(asteroides) do
		for j, tiro in ipairs(tiros) do
			if ChecaColissao(asteroide.x, asteroide.y, asteroide.img:getWidth(), asteroide.img:getHeight(), tiro.x, tiro.y, tiro.img:getWidth(), tiro.img:getHeight()) then
				table.remove(tiros, j)
				table.remove(asteroides, i)
				pontos = pontos + 100
			end
		end
		if jogador.inv == false then	
			if ChecaColissao(asteroide.x, asteroide.y, asteroide.img:getWidth(), asteroide.img:getHeight(), jogador.x, jogador.y, jogador.img:getWidth(), jogador.img:getHeight()) 
			and vivo then
				table.remove(asteroides, i)
				if shields.shield < 1 then
					vidas.vida= vidas.vida-1
				elseif shields.shield > 0 then
					shields.shield = shields.shield-1
				end
				jogador.inv = true
				tempoInv  = tempoMaxInv
				jogador.x = love.graphics:getWidth()/2-25
				jogador.y = love.graphics:getHeight()-80
				if vidas.vida == 0 then
					vivo = false
				end
			end
		end
	end

	--termina o jogo
	if not vivo then
		
		player_reset()
	
	end
end
end

function love.keypressed(key, unicode)
      if key == 'escape' and vivo == true then 
      	pause = not pause
      	historia = false
        info = false
      	hangar = false
      end
end

function love.draw(dt)
	love.graphics.setColor(255, 255, 255)
	if pause == false then
		mapa_draw()

    	love.graphics.print('Vidas: ', 670, 10)
  		if vidas.vida >= 1 then
  			love.graphics.draw(vidas.img, 711, 5)
  		end
  		if vidas.vida >= 2 then
  			love.graphics.draw(vidas.img, 731, 5)
  		end	
  		if vidas.vida >= 3 then
  			love.graphics.draw(vidas.img, 751, 5)
  		end	

    	love.graphics.print('Escudos: ', 670, 25)
  		if shields.shield >= 1 then
  			love.graphics.draw(shields.img, 725, 23)
  		end
  		if shields.shield >= 2 then
  			love.graphics.draw(shields.img, 745, 23)
  		end
  		if shields.shield >= 3 then
  			love.graphics.draw(shields.img, 765, 23)
  		end

		for i, especial in ipairs(especiais) do
			love.graphics.draw(especial.img, especial.x, especial.y)
		end

		boss_draw()

		enemy_draw()

		for i, tiro in ipairs(tiros) do
			love.graphics.draw(tiro.img, tiro.x, tiro.y)
		end

		player_draw()

		for i, explosao in ipairs(explosoes) do
			animation:draw(image, explosao.x, explosao.y)
		end
	
		if tempoMaxTiro == 0.2 then
			love.graphics.draw(Bullet2x, 9, love.graphics.getHeight() - Bullet2x:getHeight());
		end

		if doisProjetil == true then
			love.graphics.draw(maisTiro, 10+Bullet2x:getWidth() , love.graphics.getHeight() - maisTiro:getHeight())
		end

		love.graphics.print("Pontos: " .. tostring(pontos), love.graphics:getWidth()/2-10, 10)

		if debug then
			love.graphics.print("Melhor pontuação: " .. melhorPontuacao, 9, 10)
			love.graphics.print("Fase " .. level, 250, 10)
		end
	end
	botao_draw()

end