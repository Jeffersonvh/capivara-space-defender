function player_load()
	doisProjetil = false
	podeAtira=true
	tempoMaxTiro=0.4
	tempoTiro = tempoMaxTiro
	
	capivaraNave = love.graphics.newImage('pictures/nave.png')

	vidas = { vida=1, img=nil}
	vidas.img = love.graphics.newImage('pictures/heart.png')

	shields = { shield=0, img=nil}
	shields.img = love.graphics.newImage('pictures/shield.png')

	jogador = { x = love.graphics:getWidth()/2-25, y = love.graphics:getHeight()-80, velocidade = 300, img = capivaraNave, inv=false }
	vivo = false
	pontos = 0

	tiroImg = love.graphics.newImage('pictures/bullet.png')
end

function player_update(dt)
	
	--Tempo para disparar o tiro
	tempoTiro = tempoTiro - (1 * dt)
	if tempoTiro < 0 then
		podeAtira = true
	end

	for i, tiro in ipairs(tiros) do
		tiro.y = tiro.y - (250 * dt)

		if tiro.y < 0 then -- Remove o projetil que esta fora da tela
			table.remove(tiros, i)
		end
	end

	-- Movimento Horizontal
	if love.keyboard.isDown('left','a') then
		if jogador.x > 0 then -- binds us to the map
			jogador.x = jogador.x - (jogador.velocidade*dt)
		end
	elseif love.keyboard.isDown('right','d') then
		if jogador.x < (love.graphics.getWidth() - jogador.img:getWidth()) then
			jogador.x = jogador.x + (jogador.velocidade*dt)
		end
	end

	-- Movimento Vertical
	if love.keyboard.isDown('up', 'w') then
		if jogador.y > (love.graphics.getHeight() / 2) then
			jogador.y = jogador.y - (jogador.velocidade*dt)
		end
	elseif love.keyboard.isDown('down', 's') then
		if jogador.y < (love.graphics.getHeight() - 55) then
			jogador.y = jogador.y + (jogador.velocidade*dt)
		end
	end

	if love.keyboard.isDown(' ', 'rctrl', 'lctrl', 'ctrl') and podeAtira then
		-- Criando Tiros
		if doisProjetil ==false then 
			novoTiro = { x = jogador.x + (jogador.img:getWidth()/2-5), y = jogador.y, img = tiroImg }
			table.insert(tiros, novoTiro)
		end
		if doisProjetil ==true then 
			novoTiro = { x = jogador.x + (jogador.img:getWidth()/2-15), y = jogador.y, img = tiroImg }
			table.insert(tiros, novoTiro)
			novoTiro = { x = jogador.x + (jogador.img:getWidth()/2+5), y = jogador.y, img = tiroImg }
			table.insert(tiros, novoTiro)
		end

		podeAtira = false
		tempoTiro = tempoMaxTiro
	end

	if jogador.x < 0 then
		jogador.x = 0
	end
	if jogador.x > love.graphics.getWidth() then
		jogador.x = love.graphics.getWidth() - jogador.img:getWidth()
	end
	if jogador.y < love.graphics.getHeight()/2 then
		jogador.y = love.graphics.getHeight()/2
	end
	if jogador.y > love.graphics.getHeight() then
		jogador.y = love.graphics.getHeight() - jogador.img:getHeight()
	end

end

function player_reset()
	--Tirar o tiros e os inimigos e especiais
	if jogador.img == capivaraNave then
		vidas.vida = 1
	else
		for i, nave in ipairs(Naves) do
			if jogador.img == nave.img then
				vidas.vida = nave.vidas
			end
		end
	end

	if jogador.img == capivaraNave then
		shields.shield = 0
	else
		for i, nave in ipairs(Naves) do
			if jogador.img == nave.img then
				shields.shield = nave.shield
			end
		end
	end

	tempoMaxTiro=0.4
	tiros = {}
	tirosInimigo = {}
	explosoes = {}
	especiais = {}
	inimigos = {}
	asteroides = {}
	doisProjetil = false
	level = 1

	-- zerando 
	tempoTiro = tempoMaxTiro
	tempoInimigo = tempoMaxInimigo
	tempoAsteroide = tempoMaxAsteroide
	tempoInimigoH = tempoMaxInimigoH
	tempoInimigoV = tempoMaxInimigoV
	tempo_boss_time = tempo_max_boss_time
	boss_vivo = false
	InimigoVivo = false
	-- colocando o jogador na posição inicial
	jogador.x = 300
	jogador.y = 530

	if pontos >  tonumber(melhorPontuacao) then
    			melhorPontuacao = pontos
    			file = love.filesystem.newFile("score.sav")
  				file:open('w')
  				file:write(melhorPontuacao)
 				file:close()
  	end

	-- Reinicia os pontos e a vida
	pontos = 0
end

function player_draw()
	if vivo then
		if jogador.inv == true then
		love.graphics.setColor(255, 255, 255, 90)
		love.graphics.draw(jogador.img, jogador.x, jogador.y)
		love.graphics.setColor(255, 255, 255)
		else
		love.graphics.draw(jogador.img, jogador.x, jogador.y)
		end	
	end
end