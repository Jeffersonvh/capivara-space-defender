# Capivara Space Defender

O jogo foi realizado para a matéria de Projeto de Desenvolvimento de Sistemas, a proposta foi mostrar a capacidade de aprender uma nova linguagem de programação e aplicala em um programa complexo, como um jogo, a linguagem utilizada foi Lua com o auxílio da FrameWork Love2D.

## Insatalação

Basta fazer download do projeto

```bash
git clone https://gitlab.com/Jeffersonvh/capivara-space-defender.git
```

## Executar

Basta fazer download do [LöVE](https://love2d.org/) e usar ele para executar.

## Licença
[MIT](https://choosealicense.com/licenses/mit/)