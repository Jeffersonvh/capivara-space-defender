function selecionar_load()
	fundo =love.graphics.newImage('pictures/fundo.png')

	Nave_1_Img = love.graphics.newImage('pictures/Nave1.png')
	Nave1 = {x=255, y=95, img= Nave_1_Img, preco=10000, vidas=2, shield=0}

	Nave_2_Img = love.graphics.newImage('pictures/Nave2.png')
	Nave2 = {x=Nave1.x+Nave1.img:getWidth()+100, y=95, img= Nave_2_Img, preco=12000, vidas=3, shield=1}

	Nave_3_Img = love.graphics.newImage('pictures/Nave3.png')
	Nave3 = {x=Nave2.x+Nave2.img:getWidth()+100, y=95, img= Nave_3_Img, preco=24000, vidas=3, shield=3}

	Naves = {Nave1, Nave2, Nave3}
end

function selecionar_draw()
	 love.graphics.setColor(255, 255, 255, 60)
	love.graphics.draw(fundo, 200, 50)

	for i, nave in ipairs(Naves) do
	 	if jogador.img == nave.img then
	 		love.graphics.setColor(100, 255, 100)
			love.graphics.draw(nave.img, nave.x, nave.y)
			love.graphics.print('Pontos Mín.: '..nave.preco, nave.x, nave.y+nave.img:getHeight())
			love.graphics.print('Vidas: '..nave.vidas, nave.x, nave.y+nave.img:getHeight()+10)
			love.graphics.print('Escudos: '..nave.shield, nave.x, nave.y+nave.img:getHeight()+20)
	 	else
	 		love.graphics.setColor(255, 255, 255)
			love.graphics.draw(nave.img, nave.x, nave.y)
			love.graphics.print('Pontos Mín.: '..nave.preco, nave.x, nave.y+nave.img:getHeight())
			love.graphics.print('Vidas: '..nave.vidas, nave.x, nave.y+nave.img:getHeight()+10)
			love.graphics.print('Escudos: '..nave.shield, nave.x, nave.y+nave.img:getHeight()+20)
		end
	end
end