function mapa_load()
	ilha = love.graphics.newImage('pictures/ilha.png')
	ilhazinha = love.graphics.newImage('pictures/ilhazinha.png')
	ilha_esquerda = love.graphics.newImage('pictures/ilha_esquerda.png')
	ilha_direita = love.graphics.newImage('pictures/ilha_direita.png')

	ilha_inimiga = love.graphics.newImage('pictures/Ilha_inimiga.png')
	ilhazinha_inimiga = love.graphics.newImage('pictures/Ilhazinha_inimiga.png')
	ilha_direita_inimiga = love.graphics.newImage('pictures/Ilha_inimiga_esquerda.png')
	ilha_esquerda_inimiga = love.graphics.newImage('pictures/Ilha_inimiga_Direita.png')

	terra = love.graphics.newImage('pictures/terra.png')
	jupiter = love.graphics.newImage('pictures/jupiter.png')
	planeta = love.graphics.newImage('pictures/planeta.png')

	proxima_imagem = true
	imagem_atual = {}
	modesurvive = false
end

function map_update(dt)
	if modesurvive == false then 
		if level == 1 then
			if proxima_imagem == true then
				proxima_imagem = false
				numero_aleatorio = math.random(1, 4)
				if numero_aleatorio == 1 then
					imagem_atual = {img = ilha, x=150, y= 0-ilha:getHeight()}
				end
				if numero_aleatorio == 2 then
					imagem_atual = {img = ilhazinha, x=230, y= 0-ilhazinha:getHeight()}
				end
				if numero_aleatorio == 3 then
					imagem_atual = {img = ilha_esquerda, x=0, y= 0-ilha_esquerda:getHeight()}
				end
				if numero_aleatorio == 4 then
					imagem_atual = {img = ilha_direita, x=love.graphics.getWidth()-ilha_direita:getWidth(), y= 0-ilha_direita:getHeight()}
				end
			end
		end

		if level == 2 then
			if proxima_imagem == true then
				proxima_imagem = false
				numero_aleatorio = math.random(1, 3)
				if numero_aleatorio == 1 then
					imagem_atual = {img = terra, x=150, y= 0-terra:getHeight()}
				end
				if numero_aleatorio == 2 then
					imagem_atual = {img = jupiter, x=230, y= 0-jupiter:getHeight()}
				end
				if numero_aleatorio == 3 then
					imagem_atual = {img = planeta, x=0, y= 0-planeta:getHeight()}
				end
			end
		end
	
		if level == 3 then
			if proxima_imagem == true then
				proxima_imagem = false
				numero_aleatorio = math.random(1, 4)
				if numero_aleatorio == 1 then
					imagem_atual = {img = ilha_inimiga, x=150, y= 0-ilha:getHeight()}
				end
				if numero_aleatorio == 2 then
					imagem_atual = {img = ilhazinha_inimiga, x=230, y= 0-ilhazinha:getHeight()}
				end
				if numero_aleatorio == 3 then
					imagem_atual = {img = ilha_esquerda_inimiga, x=0, y= 0-ilha_esquerda:getHeight()}
				end
				if numero_aleatorio == 4 then
					imagem_atual = {img = ilha_direita_inimiga, x=love.graphics.getWidth()-ilha_direita:getWidth(), y= 0-ilha_direita:getHeight()}
				end
			end
		end
	end

	if modesurvive == true then
		if proxima_imagem == true then
			proxima_imagem = false
			numero_aleatorio = math.random(1, 3)
			if numero_aleatorio == 1 then
				imagem_atual = {img = terra, x=150, y= 0-terra:getHeight()}
			end
			if numero_aleatorio == 2 then
				imagem_atual = {img = jupiter, x=230, y= 0-jupiter:getHeight()}
			end
			if numero_aleatorio == 3 then
				imagem_atual = {img = planeta, x=0, y= 0-planeta:getHeight()}
			end
		end
	end

	if (vivo == true and not pause) then
		imagem_atual.y = imagem_atual.y + (100*dt)
	end
		
	if imagem_atual.y > love.graphics.getHeight() then
		proxima_imagem = true
	end
end

function mapa_draw()
	if vivo == true then
		if level == 1 and modesurvive == false then
			love.graphics.draw(imagem_atual.img, imagem_atual.x, imagem_atual.y)
			love.graphics.setBackgroundColor(153, 217, 234)
		end
		if level == 2 or modesurvive == true then
			love.graphics.draw(imagem_atual.img, imagem_atual.x, imagem_atual.y)
			love.graphics.setBackgroundColor(0, 0, 0)
		end
		if level == 3 and modesurvive == false then
			love.graphics.draw(imagem_atual.img, imagem_atual.x, imagem_atual.y)
			love.graphics.setBackgroundColor(153, 0, 153)
		end
	end
end