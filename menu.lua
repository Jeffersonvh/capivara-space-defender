	require 'select'
function botao_load()
	selecionar_load()

	banner = love.graphics.newImage('pictures/banner.png')
	espaco = love.graphics.newImage('pictures/Espaco.png')
	teclado = love.graphics.newImage('pictures/teclas.png')
	caixa = love.graphics.newImage('pictures/box.png')
	comfirmaImage = love.graphics.newImage('pictures/ok.png')
	mon = love.graphics.newImage('pictures/musicaon.png')
	musicaoff = love.graphics.newImage('pictures/musicaoff.png')
	hon = love.graphics.newImage('pictures/historiaon.png')
	historiaoff = love.graphics.newImage('pictures/historiaoff.png')


	musicaon ={img = mon, x=300, y=395}
	historiaon ={img = hon, x=musicaon.x+musicaon.img:getWidth()+10, y=musicaon.y}

	buttonimg = love.graphics.newImage('pictures/button.png')
	botao = {x=10, y=30, text='Play', img= buttonimg}
	botao2 = {x=10, y=botao.y+botao.img:getHeight()+50, text='Informações', img= buttonimg}
	botao3 = {x=10, y=botao2.y+botao.img:getHeight()+45, text='História', img= buttonimg}
	botao4 = {x=10, y=botao3.y+botao.img:getHeight()+45, text='Hangar', img= buttonimg}
	botao5 = {x=10, y=botao4.y+botao.img:getHeight()+45, text='Como jogar', img= buttonimg}
	botaoq = {x=10, y=botao5.y+botao.img:getHeight()+45, text='Sair', img= buttonimg}

	Botoes = {botao, botao2, botao3, botao4, botao5, botaoq}

	dialogo = {x=love.graphics.getWidth()/2-caixa:getWidth()/2, y=love.graphics.getHeight()/2-caixa:getHeight(), text='Erro não indentificado.', img=caixa}
	comfirma = {img=comfirmaImage, x=dialogo.x+dialogo.img:getWidth()-(comfirmaImage:getWidth()+10) , y=dialogo.y+dialogo.img:getHeight()-(comfirmaImage:getHeight()+10)}

	info = false
	historia = false
	hangar = false
	comoj = false
	num = 0
	sound = true

	ytexto = 110
	xtexto = 235

	descricaoAsteroide = {x= 255, y=95, pontos='Pontos: 100', vida='1 de HP',  img = asteroideImg}
	descricaoEnemy1 = {x= descricaoAsteroide.x+descricaoAsteroide.img:getWidth()+100, y=95, pontos='Pontos: 150', vida='1 de HP',  img = inimigoImg}
	descricaoEnemy2 = {x= descricaoEnemy1.x+descricaoEnemy1.img:getWidth()+100, y=95, pontos='Pontos: 200', vida='1 de HP',  img = inimigoVImg}
end

function botao_update(dt)
	if sound == true then
		if pause == true or vivo == false then
			if vivo == false then
				Batalha:stop()
			else
				Batalha:pause()
			end
			Musica:play()
		elseif pause == false or vivo == true then
			Musica:stop()
			Batalha:play()
		end
	end
end

function botao_draw()

	if pause == true or vivo == false then
		love.graphics.draw(espaco, 0, 0)
		love.graphics.draw(banner, 0, 554)
		if pause == true then
			love.graphics.print('Jogo pausado', 10, 5,  0, 1.5, 1.5)
		end
		love.graphics.setBackgroundColor(0, 0, 0)

		for i, botao in ipairs(Botoes) do
			if num == i then
				love.graphics.setColor(100, 255, 100)
				love.graphics.draw(botao.img, botao.x, botao.y)
				love.graphics.print(botao.text, botao.x+botao.img:getWidth()/2-botao.img:getWidth()/4, botao.y+botao.img:getHeight()/2-botao.img:getHeight()/6)
			else
				love.graphics.setColor(255, 255, 255)
				love.graphics.draw(botao.img, botao.x, botao.y)
				love.graphics.print(botao.text, botao.x+botao.img:getWidth()/2-botao.img:getWidth()/4, botao.y+botao.img:getHeight()/2-botao.img:getHeight()/6)
			end
		end
	end

	if info == true then
		love.graphics.print("Informações", love.graphics.getWidth()/2-40, 65, 0, 1.3, 1.3)
		love.graphics.setColor(255, 255, 255, 60)
		fundo_draw()
		
		love.graphics.draw(descricaoAsteroide.img, descricaoAsteroide.x, descricaoAsteroide.y) 
		love.graphics.print(descricaoAsteroide.pontos, descricaoAsteroide.x, descricaoAsteroide.y+descricaoAsteroide.img:getHeight())
		love.graphics.print(descricaoAsteroide.vida, descricaoAsteroide.x, descricaoAsteroide.y+descricaoAsteroide.img:getHeight()+10)
		
		love.graphics.draw(descricaoEnemy1.img, descricaoEnemy1.x, descricaoEnemy1.y) 
		love.graphics.print(descricaoEnemy1.pontos, descricaoEnemy1.x, descricaoEnemy1.y+descricaoEnemy1.img:getHeight())
		love.graphics.print(descricaoEnemy1.vida, descricaoEnemy1.x, descricaoEnemy1.y+descricaoEnemy1.img:getHeight()+10)
		
		love.graphics.draw(descricaoEnemy2.img, descricaoEnemy2.x, descricaoEnemy2.y) 
		love.graphics.print(descricaoEnemy2.pontos, descricaoEnemy2.x, descricaoEnemy2.y+descricaoEnemy2.img:getHeight())
		love.graphics.print(descricaoEnemy2.vida, descricaoEnemy2.x, descricaoEnemy2.y+descricaoEnemy2.img:getHeight()+10)

			if sound == true then
				love.graphics.draw(musicaon.img, musicaon.x, musicaon.y)
			else
				love.graphics.draw(musicaoff, musicaon.x, musicaon.y)
			end

			if modesurvive == false then
				love.graphics.draw(historiaon.img, historiaon.x, historiaon.y)
			else
				love.graphics.draw(historiaoff, historiaon.x, historiaon.y)
			end
	end

	if historia == true then
		love.graphics.setColor(255, 255, 255, 200)
		fundo_draw()
		love.graphics.print('Capivara Space defender', love.graphics.getWidth()/2-40, 90, 0, 1.3, 1.3)
		love.graphics.print(' Os seres humanos viviam em seu planeta aonde achavam que', xtexto, ytexto, 0, 1.1, 1.1)
		love.graphics.print('as capivaras eram apenas animais comuns que viviam entre,', xtexto, ytexto+15, 0, 1.1, 1.1)
		love.graphics.print('os humanos mas o que eles não sabiam é que as capivaras', xtexto, ytexto+30, 0, 1.1, 1.1)
		love.graphics.print('eram seres superiores e que estavam na terra para estudar', xtexto, ytexto+45, 0, 1.1, 1.1)
		love.graphics.print('o comportamento humano em busca de respostas sobre a', xtexto, ytexto+60, 0, 1.1, 1.1)
		love.graphics.print('base de uma civilização e a vida.', xtexto, ytexto+75, 0, 1.1, 1.1)
		love.graphics.print('Tudo ocorria muito bem como o planejado até que uma frota', xtexto, ytexto+90, 0, 1.1, 1.1)
		love.graphics.print('de vida extraterrestre tenta invadir a terra, as capivaras para',  xtexto, ytexto+105, 0, 1.1, 1.1)
		love.graphics.print('não perder toda sua pesquisa decidiram pela integridade', xtexto, ytexto+120, 0, 1.1, 1.1)
		love.graphics.print('da terra e assim chamaram o seu melhor guerreiro, "Pericles"', xtexto, ytexto+135, 0, 1.1, 1.1)
		love.graphics.print('como todos o chamam, e com essa missão o futuro da terra', xtexto, ytexto+150, 0, 1.1, 1.1)
		love.graphics.print('cesta em suas patas.', xtexto, ytexto+165, 0, 1.1, 1.1)
	end

	if hangar == true then
		love.graphics.print("Melhor pontuação: " .. melhorPontuacao, love.graphics.getWidth()/2-40, 65, 0, 1.3, 1.3)
		selecionar_draw()
	end

	if comoj == true then
		love.graphics.setColor(255, 255, 255, 60)
		fundo_draw()
		love.graphics.print("Como Jogar", love.graphics.getWidth()/2-40, 65, 0, 1.3, 1.3)
		love.graphics.draw(teclado, 255, 95) 
	end

	if alerta == true then
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(dialogo.img, dialogo.x, dialogo.y)
		love.graphics.print(dialogo.text, dialogo.x+10, dialogo.y+20, 0, 1.3, 1.3)
		love.graphics.draw(comfirma.img, comfirma.x, comfirma.y)
	end

	if win == true then
		love.graphics.print('Parabéns, você venceu!', 340, 30,  0, 1.5, 1.5)
	end

end


function fundo_draw()
	love.graphics.draw(fundo, 200, 50)
	love.graphics.setColor(255, 255, 255)
end

function button_false()
	historia = false
	info = false
	win = false
	hangar = false
	alerta = false
	comoj = false
end

function love.mousepressed(x, y, button)

	if pause == true or vivo == false then
		if button == "l" then
			for i, botao in ipairs(Botoes) do
				if x > botao.x and
					x < botao.x + botao.img:getWidth() and
					y > botao.y and
					y < botao.y + botao.img:getHeight() then
					
					num = i
					
					if i == 1 then
						if pause == true then
							historia = false
							pause = false
							info = false
							hangar = false
							alerta = false
						end
						if vivo == false then
							button_false()
							vivo = true
							tempo_boss_time = tempo_max_boss_time
							boss_vivo = false
						end
					end

					if i == 2 then
						button_false()
						info = not info
					end

					if i == 3 then
						button_false()
						historia = not historia
					end

					if i == 4 then
						button_false()
						hangar = not hangar
					end

					if i == 5 then
						button_false()
						comoj = not comoj
					end

					if i == 6 then
						love.event.push('quit')
					end
				end
			end

			if info == true then
				if x > musicaon.x and
						x < musicaon.x + musicaon.img:getWidth() and
						y > musicaon.y and
						y < musicaon.y + musicaon.img:getHeight() then
						sound = not sound
						Musica:stop()
						Batalha:stop()
				end

				if x > historiaon.x and
						x < historiaon.x + historiaon.img:getWidth() and
						y > historiaon.y and
						y < historiaon.y + historiaon.img:getHeight() then
						if pause == false then
							modesurvive = not modesurvive
							proxima_imagem = true
						else
							dialogo.text = 'Não pode trocar durante a partida!'
							alerta = true
						end
				end
			end

			for i, nave in ipairs(Naves) do
				if x > nave.x and
					x < nave.x + nave.img:getWidth() and
					y > nave.y and
					y < nave.y + nave.img:getHeight() then

					if tonumber(melhorPontuacao) >= nave.preco and pause == false then
						
						jogador.img = nave.img
						
					elseif pause == true then
							
						dialogo.text = 'Não pode trocar de nave durante a partida!'
						alerta = true
							
					elseif tonumber(melhorPontuacao) < nave.preco then
							dialogo.text = 'Sua melhor pontuação é muito baixa! =('
							alerta = true
					end
				end
			end

		end
	end

	if x > comfirma.x and
		x < comfirma.x + comfirma.img:getWidth() and
		y > comfirma.y and
		y < comfirma.y + comfirma.img:getHeight() and
		alerta == true then
		
		alerta = false

	end
end